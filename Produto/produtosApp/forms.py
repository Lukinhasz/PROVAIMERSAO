from django.forms import ModelForm
from .models import Product

class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = ['first_name', 'last_name', 'date', 'value', 'cost', 'peso',]