from django.db import models

class Product(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    date = models.IntegerField()
    value = models.CharField(max_length=5)
    cost = models.CharField(max_length=4)
    peso = models.CharField(max_length=4)

    def __str__(self):
        return self.first_name