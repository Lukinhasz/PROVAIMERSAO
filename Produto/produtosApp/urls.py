from django.urls import path
from .views import *


urlpatterns = [
    path('list/', product_list, name='Product_list'),
    path('new/', product_new, name='product_new'),
    path('update/<int:id>/', product_update, name='Product_update'),
    path('delete/<int:id>/', product_delete, name='Product_new'),

]