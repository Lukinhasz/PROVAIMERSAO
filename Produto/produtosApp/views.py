from django.shortcuts import render, redirect, get_object_or_404
from .forms import *

#CRUD
#CREATE

def product_new(request, template_name='product_form.html'):
    form = ProductForm(request.POST or None)

    dados = {'form': form}

    if form.is_valid():
        form.save()
        return redirect('product_list')
    return render(request, template_name, dados)

#READ
def product_list(request, template_name='home.html'):
    product = Product.objects.all()
    dados = {'Product': Product}
    return render(request, template_name, dados)

#UPDATE
def product_update(request, id, template_name='product_form.html'):
    product = get_object_or_404(Product, pk=id)
    form = ProductForm(request.POST or None, instance=product)

    dados = {'form': form}

    if form.is_valid():
        form.save()
        return redirect('product_list')

    return render(request, template_name, dados)

#delete
def product_delete(request, id, template_name='product_delete.html'):
    product = get_object_or_404(Product, pk=id)

    if request.method == 'POST':
        product.delete()
        return redirect('product_list')

    return render(request, template_name, {'product': product})